/*USE RutCom;*/

INSERT INTO acciones (`id`, `nombre_accion`)
VALUES 
    ('1', 'Ver'),
    ('2', 'Editar'),
    ('3', 'Prueba1'),
    ('4', 'Prueba2'),
    ('5', 'Prueba3'),
    ('6', 'Prueba4'),
    ('7', 'Prueba5'),
    ('8', 'Prueba6'),
    ('9', 'Prueba7'),
    ('10', 'Prueba8'),
    ('11', 'Prueba9'),
    ('12', 'Prueba10'),
    ('13', 'Prueba11'),
    ('14', 'Prueba12'),
    ('15', 'Prueba13');

INSERT INTO perfiles (`id`, `nombre_perfil`) 
VALUES ('1', 'Administrador');

INSERT INTO turnos (`id`, `dia`, `hora_entrada`, `hora_salida`) 
VALUES ('1', '1', '14:00', '22:00');

INSERT INTO turnos (`id`, `dia`, `hora_entrada`, `hora_salida`) 
VALUES ('2', '2', '14:00', '22:00');

INSERT INTO turnos (`id`, `dia`, `hora_entrada`, `hora_salida`) 
VALUES ('3', '3', '14:00', '22:00');

INSERT INTO turnos (`id`, `dia`, `hora_entrada`, `hora_salida`) 
VALUES ('4', '4', '14:00', '22:00');

INSERT INTO turnos (`id`, `dia`, `hora_entrada`, `hora_salida`) 
VALUES ('5', '5', '14:00', '22:00');

INSERT INTO horarios (`id`, `nombre_horario`) 
VALUES ('1', 'Tarde');

INSERT INTO categorias_profesionales (`id`, `nombre_categoria`) 
VALUES ('1', 'Director');

INSERT INTO medios_comunicacion (`id`, `nombre_medio_comunicacion`) 
VALUES ('1', 'Llamada telefónica');

INSERT INTO medios_comunicacion (`id`, `nombre_medio_comunicacion`) 
VALUES ('2', 'SMS');

INSERT INTO medios_comunicacion (`id`, `nombre_medio_comunicacion`) 
VALUES ('3', 'Email');

INSERT INTO categoria_clientes (`id`, `nombre_categoria`) 
VALUES ('1', 'VIP');

INSERT INTO categoria_clientes (`id`, `nombre_categoria`) 
VALUES ('2', 'Urgente');

INSERT INTO tags (`id`, `nombre`)
VALUES ('1', 'Domicilio Admin');
INSERT INTO tags (`id`, `nombre`) 
VALUES ('2', 'Hotel ITEC');

INSERT INTO permisos (`id`, `nombre_permiso`, `fk_id_perfil`, `fk_id_accion`) 
VALUES ('1', 'Emails', '1', '2');

INSERT INTO permisos (`id`, `nombre_permiso`, `fk_id_perfil`, `fk_id_accion`) 
VALUES ('2', 'Rutas', '1', '2');

INSERT INTO personas (`id`, `nombre_persona`, `apellidos_persona`, `dninie`, `nss`, `foto`, `fk_id_categoria_profesional`, `fk_id_horario`)
VALUES ('1', 'Alberto', 'Sanchez', '11223344S', '102030405060', 'foto1.jpg', '1', '1');

INSERT INTO usuarios (`id`, `pass`, `nombre_usuario`, `fk_id_persona`, `facesign`)
VALUES ('1', 'aW40amRNUFY2WVFaWWtNOHJUZG1sUT09', 'User', '1','');