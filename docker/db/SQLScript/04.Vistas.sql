DROP VIEW IF EXISTS PersonasView;
CREATE VIEW PersonasView
AS SELECT 
	personas.nombre_persona as nombre_personaPersonas,
    personas.apellidos_persona as apellidos_personaPersonas,
    personas.dninie as dniniePersonas,
    personas.nss as nssPersonas,
    personas.foto as fotoPersonas,
    telefonos.numero_telefono as numero_telefonoPersonas,
    emails.direccion_email as direccion_emailPersonas,
    ubicaciones.latitud as latitudPersonas,
    ubicaciones.longitud as longitudPersonas,
    ubicaciones.nombre_ubicacion as nombre_ubicacionPersonas,
    tags.nombre as nombre_tagPersonas,
    usuarios.nombre_usuario as nombre_usuarioPersonas,
    perfiles.nombre_perfil as nombre_perfilPersonas,
    permisos.nombre_permiso as nombre_permisoPersonas,
    acciones.nombre_accion as nombre_accionPersonas,
    horarios.nombre_horario as nombre_horarioPersonas,
    turnos.dia as dia_turnoPersonas,
    turnos.hora_entrada as hora_entrada_turnoPersonas,
    turnos.hora_salida as hora_salida_turnoPersonas,
    categorias_profesionales.nombre_categoria as nombre_categoriaPersonas,
    controles_horarios.fecha_hora_entrada as fecha_hora_entradaPersonas,
    controles_horarios.fecha_hora_salida as fecha_hora_salidaPersonas
FROM personas
INNER JOIN telefonos on personas.id = telefonos.fk_id_persona
INNER JOIN emails on personas.id = emails.fk_id_persona
INNER JOIN ubicaciones on personas.id = ubicaciones.fk_id_persona
INNER JOIN ubicacionFija_tag on ubicaciones.nombre_ubicacion = ubicacionFija_tag.fk_nombre_ubicacion
INNER JOIN tags on ubicacionFija_tag.fk_id_tag = tags.id
INNER JOIN usuarios on personas.id = usuarios.fk_id_persona
INNER JOIN usuarios_perfiles on usuarios.id = usuarios_perfiles.fk_id_usuario
INNER JOIN perfiles on usuarios_perfiles.fk_id_perfil = perfiles.id
INNER JOIN permisos on perfiles.id = permisos.fk_id_perfil
INNER JOIN acciones on permisos.fk_id_accion = acciones.id
INNER JOIN horarios on horarios.id = personas.fk_id_horario
INNER JOIN horarios_turnos on horarios.id = horarios_turnos.fk_id_horario
INNER JOIN turnos on horarios_turnos.fk_id_turno = turnos.id
INNER JOIN categorias_profesionales on personas.fk_id_categoria_profesional = categorias_profesionales.id
INNER JOIN controles_horarios on  personas.id = controles_horarios.fk_id_persona; 

CREATE VIEW ComunicacionesView
AS Select 
	comunicaciones.fecha_hora_comunicacion as fechaComunicacion, 
    comunicaciones.contenido as contenidoComunicacion, 
    personas.nombre_persona as nombre_personaComunicacion, 
    personas.apellidos_persona as apellidos_personaComunicacion,
	clientes.nombre_cliente as clientes_nombreComunicacion,
    clientes.apellidos_cliente as clientes_apellidoComunicacion,
    medios_comunicacion.nombre_medio_comunicacion as medioComunicacion,
    recordatorios.fecha_hora_creacion as recordatorios_FechaCreacionComunicacion,
    recordatorios.fecha_hora_aviso as recordatorios_FechaAvisoComunicacion,
    recordatorios.texto as recordatorios_TextComunicacion
from comunicaciones
inner join personas_comunicaciones on comunicaciones.id = personas_comunicaciones.fk_id_comunicacion
inner join personas on personas_comunicaciones.fk_id_persona = personas.id
inner join clientes on comunicaciones.fk_id_cliente = clientes.id
inner join medios_comunicacion on comunicaciones.fk_id_medio_comunicacion = medios_comunicacion.id
inner join recordatorios on	comunicaciones.fk_id_recordatorio = recordatorios.id;

CREATE VIEW EventosView
AS Select 
	eventos.nombre_evento as nombreEvento, 
    eventos.fecha_hora_evento as fechahoraEvento,
    eventos.anotaciones as anotacionesEvento,
    personas.nombre_persona as nombre_personaEvento, 
    personas.apellidos_persona as apellidos_personaEvento,
	clientes.nombre_cliente as nombre_clienteEvento,
    clientes.apellidos_cliente as apellidos_clienteEvento,
    ubicaciones.latitud as latitudEvento,
    ubicaciones.longitud as longitudEvento,
    ubicaciones.nombre_ubicacion as nombre_ubicacionEvento,
    informes.comentario as comentarioEvento,
    recordatorios.texto as recordatorios_TextEvento
from eventos
inner join personas_eventos on eventos.id = personas_eventos.fk_id_evento
inner join personas on personas_eventos.fk_id_persona = personas.id
inner join clientes_eventos on eventos.id = clientes_eventos.fk_id_evento
inner join clientes on clientes_eventos.fk_id_cliente = clientes.id
inner join ubicaciones on eventos.fk_nombre_ubicacion = ubicaciones.nombre_ubicacion
inner join informes on eventos.id = informes.fk_id_evento
inner join recordatorios on	eventos.id = recordatorios.fk_id_evento;

CREATE VIEW ClientesView
AS Select
	clientes.nombre_cliente as nombreCliente,
    clientes.apellidos_cliente as apellidosCliente,
    clientes.dnicif as dnicifCliente,
    telefonos.numero_telefono as telefonoCliente,
    emails.direccion_email as emailCliente,
    categoria_clientes.nombre_categoria as categoriaCliente,
    comunicaciones.contenido as comunicaciones_ContenidoCliente,
    comunicaciones.fecha_hora_comunicacion as comunicaciones_Fecha_hora_ComunicacionCliente,
    medios_comunicacion.nombre_medio_comunicacion as medio_comunicacionCliente,
    eventos.nombre_evento as eventos_nombreCliente,
    ubicaciones.latitud as ubicaciones_latitudCliente,
    ubicaciones.longitud as ubicaciones_longitudCliente,
    ubicaciones.nombre_ubicacion as ubicaciones_nombreUbicacionCliente,
    tags.nombre as tag_nombreCliente
    from clientes
    inner join telefonos on clientes.id = telefonos.fk_id_cliente
    inner join emails on clientes.fk_id_categoria_cliente = emails.fk_id_cliente
    inner join categoria_clientes on clientes.fk_id_categoria_cliente = categoria_clientes.id
    inner join comunicaciones on clientes.id = comunicaciones.fk_id_cliente
    inner join medios_comunicacion on comunicaciones.fk_id_medio_comunicacion = medios_comunicacion.id
    inner join ubicaciones on clientes.id = ubicaciones.fk_id_cliente
    inner join eventos on ubicaciones.nombre_ubicacion = eventos.fk_nombre_ubicacion
    inner join ubicacionFija_tag on ubicaciones.nombre_ubicacion = ubicacionFija_tag.fk_nombre_ubicacion
    inner join tags on ubicacionFija_tag.fk_id_tag = tags.id;
    
CREATE VIEW RutasView
AS SELECT
	rutas.nombre as nombreRutas,
    personas.nombre_persona as nombre_personaRutas,
    personas.apellidos_persona as apellidos_personaRutas,
    ubicaciones.latitud as latitud_ubicacionRutas,
    ubicaciones.longitud as longitud_ubicacionRutas,
    ubicaciones.nombre_ubicacion as nombre_ubicacionRutas,
    ubicaciones.detalle_ubicacion as detalle_ubicacionRutas,
    ubicaciones.direccion_postal as direccion_postal_ubicacionRutas,
    ubicaciones.fecha_hora as fecha_hora_ubicacionRutas,
    ubicaciones.es_cambiante as es_cambiante_ubicacionRutas,
    tags.nombre as nombre_tagsRutas
FROM rutas
INNER JOIN persona_recorre_ruta on rutas.id =  persona_recorre_ruta.fk_id_ruta
INNER JOIN personas on persona_recorre_ruta.fk_id_persona = personas.id
INNER JOIN rutas_ubicaciones on rutas.id = rutas_ubicaciones.fk_id_ruta
INNER JOIN ubicaciones on rutas_ubicaciones.fk_nombre_ubicacion = ubicaciones.nombre_ubicacion
INNER JOIN ubicacionFija_tag on ubicaciones.nombre_ubicacion = ubicacionFija_tag.fk_nombre_ubicacion
INNER JOIN tags on ubicacionFija_tag.fk_id_tag = tags.id;

CREATE VIEW LoginView
AS SELECT 
    usuarios.id as idUsuario,
    usuarios.nombre_usuario as nombre_usuario,
    usuarios.pass as pass,
    perfiles.nombre_perfil as nombre_perfil,
    personas.nombre_persona as nombre_persona,
    personas.dninie as dninie,
    permisos.nombre_permiso as nombre_permiso
FROM usuarios
INNER JOIN usuarios_perfiles on usuarios.id = usuarios_perfiles.fk_id_usuario
INNER JOIN perfiles on usuarios_perfiles.fk_id_perfil = perfiles.id
INNER JOIN personas on usuarios.fk_id_persona = personas.id
INNER JOIN permisos on perfiles.id = permisos.fk_id_perfil;
    