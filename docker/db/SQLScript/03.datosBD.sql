/*USE RutCom;*/

INSERT INTO `clientes` (`id`, `nombre_cliente`, `apellidos_cliente`, `dnicif`, `fk_id_categoria_cliente`) 
VALUES ('1', 'David', 'Rubio Atroche', '11223344A', '1');

INSERT INTO `clientes` (`id`, `nombre_cliente`, `apellidos_cliente`, `dnicif`, `fk_id_categoria_cliente`) 
VALUES ('2', 'Ignacio', 'Nuñez Rodriguez', '44332211B', '2');

INSERT INTO `telefonos` (`numero_telefono`, `es_de_persona`, `fk_id_persona`) 
VALUES ('+34 111 22 33 44', '1', '1');

INSERT INTO `telefonos` (`numero_telefono`, `es_de_persona`, `fk_id_cliente`) 
VALUES ('+34 222 33 44 55', '0', '1');

INSERT INTO `emails` (`direccion_email`, `es_de_persona`, `fk_id_persona`) 
VALUES ('admin@admin.com', '1', '1');

INSERT INTO `emails` (`direccion_email`, `es_de_persona`, `fk_id_cliente`) 
VALUES ('cliente@cliente.com', '0', '1');

INSERT INTO `controles_horarios` (`id`, `fecha_hora_entrada`, `fecha_hora_salida`, `fk_id_persona`) 
VALUES ('1', '2020-03-19 14:02:21', '2020-03-19 21:55:46', '1');

INSERT INTO `rutas` (`id`, `nombre`, `fk_id_persona_crea`) 
VALUES ('1', 'ruta_norte', '1');

SET FOREIGN_KEY_CHECKS=0;

INSERT INTO `comunicaciones` (`id`, `fecha_hora_comunicacion`, `contenido`, `fk_id_cliente`, `fk_id_medio_comunicacion`, `fk_id_recordatorio`) 
VALUES ('1', '2020-03-19 13:00:00', 'Cliente quiere reunirse el dia 21 a las 17h', '1', '1', '1');

INSERT INTO `eventos` (`id`, `nombre_evento`, `fecha_hora_evento`, `anotaciones`, `fk_latitud_ubicacion`, `fk_longitud_ubicacion`, `fk_nombre_ubicacion`, `fk_id_persona`) 
VALUES (1, 'reunion1', '2020-03-21 17:00:00', 'Puntualidad maxima', '1', '1', 'ubicacion1', '1');

INSERT INTO `eventos` (`id`, `nombre_evento`, `fecha_hora_evento`, `anotaciones`, `fk_latitud_ubicacion`, `fk_longitud_ubicacion`, `fk_nombre_ubicacion`, `fk_id_persona`) 
VALUES (2, 'reunion2', '2019-03-21 00:00:00', 'Puntualidad maxima', '1', '1', 'ubicacion2', '1');

INSERT INTO `ubicaciones` (`latitud`, `longitud`, `nombre_ubicacion`, `detalle_ubicacion`, `direccion_postal`, `es_cambiante`, `fk_id_cliente`, `fk_id_persona`, `fk_id_evento`) 
VALUES ('1.27', '-13.42', 'ubicacion1', 'hotel ITEC', '29200', '0', '1', '1', '1');

INSERT INTO `ubicaciones` (`latitud`, `longitud`, `nombre_ubicacion`, `detalle_ubicacion`, `direccion_postal`, `es_cambiante`, `fk_id_cliente`, `fk_id_persona`, `fk_id_evento`) 
VALUES ('37.0194', '-4.56289', 'ubicacion2', 'hotel ITEC 2', '29200', '0', '1', '1', '1');

INSERT INTO `informes` (`id`, `comentario`, `fk_id_persona`, `fk_id_evento`) 
VALUES ('1', 'El evento en el hotel ITEC ha sido satisfactorio', '1', '1');

INSERT INTO `recordatorios` (`id`, `texto`, `fecha_hora_creacion`, `fecha_hora_aviso`, `fk_id_evento`, `fk_id_persona`) 
VALUES ('1', 'Recordar reunión en el hotel ITEC a las 17:00', '2020-03-19 14:00:00', '2020-03-21 16:00:00', '1', '1');

INSERT INTO `clientes_eventos` (`fk_id_cliente`, `fk_id_evento`) VALUES ('1', '1');

INSERT INTO `horarios_turnos` (`fk_id_horario`, `fk_id_turno`) VALUES ('1', '1');
INSERT INTO `horarios_turnos` (`fk_id_horario`, `fk_id_turno`) VALUES ('1', '2');
INSERT INTO `horarios_turnos` (`fk_id_horario`, `fk_id_turno`) VALUES ('1', '3');
INSERT INTO `horarios_turnos` (`fk_id_horario`, `fk_id_turno`) VALUES ('1', '4');
INSERT INTO `horarios_turnos` (`fk_id_horario`, `fk_id_turno`) VALUES ('1', '5');

INSERT INTO `persona_recorre_ruta` (`fk_id_persona`, `fk_id_ruta`) VALUES ('1', '1');

INSERT INTO `personas_comunicaciones` (`fk_id_persona`, `fk_id_comunicacion`) VALUES ('1', '1');

INSERT INTO `personas_eventos` (`fk_id_persona`, `fk_id_evento`) VALUES ('1', '1');

INSERT INTO `rutas_ubicaciones` (`fk_id_ruta`, `fk_latitud_ubicacion`, `fk_longitud_ubicacion`, `fk_nombre_ubicacion`, `orden`) VALUES ('1', '1.27', '-13.42', 'ubicacion1', '1');

INSERT INTO `ubicacionFija_tag` (`fk_latitud_ubicacion`, `fk_longitud_ubicacion`, `fk_nombre_ubicacion`, `fk_id_tag`) VALUES ('1.27', '-13.42', 'ubicacion1', '2');

INSERT INTO `usuarios_perfiles` (`fk_id_usuario`, `fk_id_perfil`) VALUES ('1', '1');

SET FOREIGN_KEY_CHECKS=1;