use RutCom;
/*Para crear y hacer una instalación limpia de una base de datos*/ 
/*drop database if exists RutCom; 
create database RutCom;  
*/
/*Borra si existe usuario y Crea un usuario nuevo con varios permisos para nuestra nueva base de datos*/ 
drop user if exists 'RutComUser'@'localhost'; 
create user 'RutComUser'@'localhost' identified by '1234'; 

grant select, delete, insert, update on RutCom.* to 'RutComUser'@'localhost';  

/*Crear tablas y columnas en la base de datos*/ 


CREATE TABLE `usuarios` ( 
	id int auto_increment, 
	pass varchar(128) not null,
    facesign text,
    nombre_usuario varchar(255) UNIQUE not null,
    fk_id_persona int not null,
    PRIMARY KEY (id) 
);  

create table personas (
	id int auto_increment, 
    nombre_persona varchar(50) not null,
    apellidos_persona varchar(100) not null,
    dninie varchar(9) not null unique, 
    nss varchar(12) not null unique, 
    foto varchar(255), 
    fk_id_categoria_profesional int not null,
    fk_id_horario int not null, 
    PRIMARY KEY (id) 
); 

create table permisos ( 
	id int auto_increment, 
    nombre_permiso varchar(50) unique not null, 
    fk_id_perfil int not null, 
    fk_id_accion int not null, 
    PRIMARY KEY (id) 
); 

create table acciones ( 
	id int auto_increment, 
    nombre_accion varchar(50) unique not null, 
    PRIMARY KEY (id) 
);  

/*A los campos de fk_id_persona y fk_id_cliente le hemos quitado el not null, ya que uno de los dos campos 
siempre estará vacío.*/
create table telefonos ( 
	numero_telefono varchar(30) unique, 
    es_de_persona bool not null, 
    fk_id_persona int, 
    fk_id_cliente int, 
    CHECK (numero_telefono regexp '[+(\+[0-9][0-9])[:space:][0-9][0-9][0-9][:space:][0-9][0-9][:space:][0-9][0-9][:space:][0-9][0-9]]'), 
    PRIMARY KEY (numero_telefono) 
);  

/*A los campos de fk_id_persona y fk_id_cliente le hemos quitado el not null, ya que uno de los dos campos 
siempre estará vacío.*/
create table emails ( 
	direccion_email varchar(50) unique, 
    es_de_persona bool not null, 
    fk_id_persona int, 
    fk_id_cliente int, 
    PRIMARY KEY (direccion_email) 
);  

create table categorias_profesionales ( 
	id int auto_increment, 
    nombre_categoria varchar(50) unique not null, 
    PRIMARY KEY (id) 
);  

create table perfiles ( 
	id int auto_increment, 
    nombre_perfil varchar(50) unique not null, 
    PRIMARY KEY (id) 
);  

create table controles_horarios ( 
	id int auto_increment, 
    fecha_hora_entrada datetime not null, 
    fecha_hora_salida datetime not null, 
    fk_id_persona int not null, 
    PRIMARY KEY (id) 
);  

create table horarios ( 
	id int auto_increment, 
    nombre_horario varchar(50) unique not null, 
    PRIMARY KEY (id) 
);  

/*Hemos establecido el tipo de dato del campo día como tinyint positivo en lugar de varchar, ya que de este modo 
será más facil compararlo en un futuro */
create table turnos ( 
	id int auto_increment, 
    dia tinyint unsigned not null, 
    hora_entrada time not null, 
    hora_salida time not null, 
    PRIMARY KEY (id) 
);  

create table eventos ( 
	id int auto_increment, 
    nombre_evento varchar(100) not null, 
    fecha_hora_evento datetime not null,   
    anotaciones text, 
    fk_latitud_ubicacion float not null, 
    fk_longitud_ubicacion float not null,
    fk_nombre_ubicacion varchar(100) unique not null,
    fk_id_persona int not null, 
    PRIMARY KEY (id) 
); 

create table recordatorios ( 
	id int auto_increment, 
    texto text not null, 
    fecha_hora_creacion datetime not null, 
    fecha_hora_aviso datetime not null, 
    fk_id_evento int not null, 
    fk_id_persona int not null, 
    PRIMARY KEY (id) 
);  

create table comunicaciones ( 
	id int auto_increment, 
    fecha_hora_comunicacion datetime not null, 
    contenido text not null, /*TODO Se podría crear una nueva tabla de plantillas con mensajes predefinidos Ej: Todo bien, Estoy conduciendo, Te llamo luego... etc*/ 
    fk_id_cliente int not null, 
    fk_id_medio_comunicacion int not null, 
    fk_id_recordatorio int not null, 
    PRIMARY KEY (id) 
); 

create table medios_comunicacion ( 
	id int auto_increment, 
    nombre_medio_comunicacion varchar(50) unique not null, 
    PRIMARY KEY (id) 
);  

create table clientes ( 
	id int auto_increment, 
    nombre_cliente varchar(50) not null, 
    apellidos_cliente varchar(100) not null, 
    dnicif varchar(9) not null unique, 
    fk_id_categoria_cliente int not null, 
    PRIMARY KEY (id) 
);  

create table categoria_clientes ( 
	id int auto_increment, 
    nombre_categoria varchar(50) unique not null, 
    PRIMARY KEY (id) 
); 
 
/*Es necesario crear un INDEX para las primary keys de la tabla ubicaciones ya que si no lo hacemos
da fallo en la creación.
Es necesario eliminar not null del campo fecha_hora ya que este solo se rellenara si el campo es_cambiante es TRUE.
*/
create table ubicaciones ( 
	latitud float not null, 
    longitud float not null, 
    nombre_ubicacion varchar(100) unique not null, 
    detalle_ubicacion text not null, /*CAMBIADO!*/ 
    direccion_postal varchar(250) not null, 
    fecha_hora datetime, 
    es_cambiante bool not null, 
    fk_id_cliente int not null, 
    fk_id_persona int not null, 
    fk_id_evento int not null,    
    PRIMARY KEY (latitud, longitud, nombre_ubicacion),
    INDEX indexlatitud (latitud asc),
    INDEX indexlongitud (longitud asc),
    INDEX indexnombreubicacion (nombre_ubicacion asc) 
);  

create table tags ( 
	id int auto_increment, 
    nombre varchar(50) unique not null, 
    PRIMARY KEY (id) 
); 

create table rutas ( 
	id int auto_increment, 
    nombre varchar(100) unique not null, 
    fk_id_persona_crea int not null, 
    PRIMARY KEY (id) 
);  

create table informes ( 
	id int auto_increment, 
    comentario text not null, 
    fk_id_persona int not null, 
    fk_id_evento int not null, 
    PRIMARY KEY (id) 
); 

create table persona_recorre_ruta( 
	fk_id_persona int not null, /*PREGUNTAR SI HAY QUE PONER AMBAS COMO KEY*/ 
    fk_id_ruta int not null,
    PRIMARY KEY (fk_id_persona, fk_id_ruta)
);  

create table personas_eventos ( 
	fk_id_persona int not null, 
    fk_id_evento int not null, 
    PRIMARY KEY (fk_id_persona, fk_id_evento)
);  

create table personas_comunicaciones ( 
	fk_id_persona int not null, 
    fk_id_comunicacion int not null,
    PRIMARY KEY (fk_id_persona, fk_id_comunicacion)
);  

create table rutas_ubicaciones ( 
	fk_id_ruta int not null, 
    fk_latitud_ubicacion float not null, 
    fk_longitud_ubicacion float not null,
    fk_nombre_ubicacion varchar(100) unique not null,
    orden tinyint unsigned not null, 																		/*BUSCAR TIPO DE DATO DE 0 A 255*/
    PRIMARY KEY (fk_id_ruta, fk_latitud_ubicacion, fk_longitud_ubicacion, fk_nombre_ubicacion)    
);  

create table ubicacionFija_tag ( 
	fk_latitud_ubicacion float not null, 
    fk_longitud_ubicacion float not null, 
	fk_nombre_ubicacion varchar(100) unique not null, 
    fk_id_tag int not null,
    PRIMARY KEY (fk_latitud_ubicacion, fk_longitud_ubicacion, fk_nombre_ubicacion, fk_id_tag)
); 

create table horarios_turnos ( 
	fk_id_horario int not null, 
    fk_id_turno int not null,
    PRIMARY KEY (fk_id_horario, fk_id_turno)
); 

create table usuarios_perfiles ( 
	fk_id_usuario int not null, 
    fk_id_perfil int not null,
    PRIMARY KEY (fk_id_usuario, fk_id_perfil)
);  

create table clientes_eventos ( 
	fk_id_cliente int not null, 
    fk_id_evento int not null,
    PRIMARY KEY (fk_id_cliente, fk_id_evento)
); 

alter table usuarios 
add constraint fk_id_persona_cons0 
foreign key (fk_id_persona) 
references personas(id);  

alter table personas 
add constraint fk_id_categoria_profesional_cons 
foreign key (fk_id_categoria_profesional) 
references categorias_profesionales(id); 
alter table personas 
add constraint fk_id_horario_cons1 
foreign key (fk_id_horario) 
references horarios(id);  

alter table permisos
add constraint fk_id_perfil_cons1 
foreign key (fk_id_perfil) 
references perfiles(id); 
Alter table permisos 
Add constraint fk_id_accion_cons 
Foreign key (fk_id_accion) 
References acciones(id);  

alter table telefonos 
add constraint fk_id_persona_cons1
foreign key (fk_id_persona) 
references personas(id); 
alter table telefonos 
add constraint fk_id_cliente_cons1 
foreign key (fk_id_cliente) 
references clientes(id);  

alter table emails 
add constraint fk_id_persona_cons2 
foreign key (fk_id_persona) 
references personas(id); 
alter table emails 
add constraint fk_id_cliente_cons2 
foreign key (fk_id_cliente) 
references clientes(id);  

alter table controles_horarios 
add constraint fk_id_persona_cons3 
foreign key (fk_id_persona) 
references personas(id);  

alter table eventos 
add constraint fk_latitud_ubicacion_cons1 
foreign key (fk_latitud_ubicacion) 
references ubicaciones(latitud);
alter table eventos 
add constraint fk_longitud_ubicacion_cons1 
foreign key (fk_longitud_ubicacion) 
references ubicaciones(longitud); 
alter table eventos 
add constraint fk_nombre_ubicacion_cons1 
foreign key (fk_nombre_ubicacion) 
references ubicaciones(nombre_ubicacion); 
alter table eventos   
add constraint fk_id_persona_cons4 
foreign key (fk_id_persona) 
references personas(id);

alter table recordatorios 
add constraint fk_id_evento_cons1 
foreign key (fk_id_evento) 
references eventos(id); 
alter table recordatorios 
add constraint fk_id_persona_cons5 
foreign key (fk_id_persona) 
references personas(id);  

alter table comunicaciones 
add constraint fk_id_cliente_cons3 
foreign key (fk_id_cliente) 
references clientes(id); 
alter table comunicaciones 
add constraint fk_id_medio_comunicacion_cons 
foreign key (fk_id_medio_comunicacion) 
references medios_comunicacion(id); 
alter table comunicaciones 
add constraint fk_id_recordatorio_cons 
foreign key (fk_id_recordatorio) 
references recordatorios(id);  

alter table clientes 
add constraint fk_id_categoria_cliente_cons 
foreign key (fk_id_categoria_cliente) 
references categoria_clientes(id);  

alter table ubicaciones 
add constraint fk_id_cliente_cons4 
foreign key (fk_id_cliente) 
References clientes(id); 
alter table ubicaciones 
add constraint fk_id_persona_cons6 
foreign key (fk_id_persona) 
references personas(id); 
alter table ubicaciones 
add constraint fk_id_evento_cons2 
foreign key (fk_id_evento) 
references eventos(id);  

alter table rutas 
add constraint fk_id_persona_cons7 
foreign key (fk_id_persona_crea) 
references personas(id); 

alter table informes 
add constraint fk_id_persona_cons8 
foreign key (fk_id_persona) 
references personas(id); 
Alter table informes 
Add constraint fk_id_evento_cons3 
Foreign key (fk_id_evento) 
References eventos(id);  

Alter table persona_recorre_ruta 
Add constraint fk_id_persona_cons11 
Foreign key (fk_id_persona) 
References personas(id); 
Alter table persona_recorre_ruta 
Add constraint fk_id_ruta_cons2 
Foreign key (fk_id_ruta) 
References rutas(id);

alter table personas_eventos 
add constraint fk_id_persona_cons10 
foreign key (fk_id_persona) 
references personas(id); 
alter table personas_eventos 
add constraint fk_id_evento_cons5 
foreign key (fk_id_evento) 
references eventos(id);

alter table personas_comunicaciones 
add constraint fk_id_persona_cons9 
foreign key (fk_id_persona) 
references personas(id); 
alter table personas_comunicaciones 
add constraint fk_id_comunicacion_cons 
foreign key (fk_id_comunicacion) 
references comunicaciones(id);

Alter table rutas_ubicaciones 
Add constraint fk_id_ruta_cons1 
Foreign key (fk_id_ruta) 
References rutas(id);
Alter table rutas_ubicaciones 
Add constraint fk_latitud_ubicacion_cons3 
Foreign key (fk_latitud_ubicacion) 
References ubicaciones(latitud);
Alter table rutas_ubicaciones 
Add constraint fk_longitud_ubicacion_cons3 
Foreign key (fk_longitud_ubicacion) 
References ubicaciones(longitud); 
Alter table rutas_ubicaciones 
Add constraint fk_nombre_ubicacion_cons3 
Foreign key (fk_nombre_ubicacion) 
References ubicaciones(nombre_ubicacion);

Alter table ubicacionFija_tag 
Add constraint fk_latitud_ubicacion_cons2 
Foreign key (fk_latitud_ubicacion) 
References ubicaciones(latitud);
Alter table ubicacionFija_tag 
Add constraint fk_longitud_ubicacion_cons2 
Foreign key (fk_longitud_ubicacion) 
References ubicaciones(longitud); 
Alter table ubicacionFija_tag 
Add constraint fk_nombre_ubicacion_cons2 
Foreign key (fk_nombre_ubicacion) 
References ubicaciones(nombre_ubicacion); 
Alter table ubicacionFija_tag 
Add constraint fk_id_tag_cons 
Foreign key (fk_id_tag) 
References tags(id);

Alter table horarios_turnos 
Add constraint fk_id_horario_cons2 
Foreign key (fk_id_horario) 
References horarios(id); 
Alter table horarios_turnos 
Add constraint fk_id_turno_cons 
Foreign key (fk_id_turno) 
References turnos(id); 

Alter table usuarios_perfiles 
Add constraint fk_id_usuario_cons 
Foreign key (fk_id_usuario) 
References usuarios(id); 
Alter table usuarios_perfiles 
Add constraint fk_id_perfil_cons2 
Foreign key (fk_id_perfil) 
References perfiles(id);  

Alter table clientes_eventos  
Add constraint fk_id_cliente_cons5 
Foreign key (fk_id_cliente) 
References clientes(id); 
Alter table clientes_eventos 
Add constraint fk_id_evento_cons4 
Foreign key (fk_id_evento) 
References eventos(id);