<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\getDataItec;
use App\Http\Controllers\getInterfaceDescriptor;
use App\Http\Controllers\ClientesResource;


Route::get('/',function(){
    return view("portada");
});
Route::resource("/clientes",ClientesResource::class);


