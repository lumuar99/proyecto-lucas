@extends('layout.layout')
@section('contenido')
    @includeWhen($mode=="showall","content.clientesViewAll")
    @includeWhen($mode=="show","content.clientesView")
    @includeWhen($mode=="edit","content.editClientesView")
@endsection
