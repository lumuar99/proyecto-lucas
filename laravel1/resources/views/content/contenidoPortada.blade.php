<div class="container">
<div class="row">
    <div class="card-deck">
        @foreach ($tarjetas as $tarjeta)
        <?php
            $url = $tarjeta["enlace"];
            $titulo = $tarjeta["titulo"];
            $contenido = $tarjeta["contenido"];
            $urlBoton = $tarjeta["urlBoton"];
        ?>
        <div class="card shadow-sm">
            <iframe src="https://www.youtube.com/embed/{{$url}}"
                allow="accelerometer; autoplay; clipboard-write;
                encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen="" frameborder="0"></iframe>
            <h2>{{$titulo}}</h2>
            <div class="card-body">
                <p class="card-text">{{$contenido}}</p>
            </div>
            <div class="card-footer">
                <a class="w-100 btn btn-secondary" href="{{$urlBoton}}"
                    role="button">Ver Comentarios</a>
            </div>
        </div>
        @endforeach
    </div>
</div>

