<div class="container">
<iframe src="https://www.youtube.com/embed/{{$detallesTarjeta["enlace"]}}"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen="" width="1000px" height="500px" frameborder="0"></iframe>
    <h4 class="card-title">{{$detallesTarjeta["titulo"]}}</h4>
    <p class="card-text">{{$detallesTarjeta["contenido"]}}</p>
    <table class="table">
    @foreach ($detallesTarjeta["comentarios"] as $key=>$value)
        <tr>
            <td>{{$key}}</td>
            <td>{{$value}}</td>
        </tr>
    @endforeach
    <table>
    <p id="textoFooter"><a id="aTextoFooter" class="btn btn-secondary" href="{{url("/")}}" role="button">Volver a Inicio</a></p>
</div>
