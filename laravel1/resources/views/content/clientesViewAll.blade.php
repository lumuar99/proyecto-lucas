<div class="card-deck">
    @foreach ($data as $cliente)
        <div class="card mb-4 box-shadow">
            <div class="card-body">

                <div class="z-depth-1-halfmap-container w100">
                    <?php
                        $url="https://maps.google.com/?ll=".$cliente['ubicaciones_latitudCliente'].",".
                            $cliente['ubicaciones_longitudCliente']. "&z=14&t=m&output=embed";
                    ?>
                    <iframe src="{{$url}}" class="w-100" frameborder="0"></iframe>
                </div>

                <h5 class="card-title">{{$cliente['nombreCliente']}} {{$cliente['apellidosCliente']}}</h5>
                <p class="card-text">{{$cliente['dnicifCliente']}}</p>
                <p class="card-text">Categoria: {{$cliente['categoriaCliente']}}</p>
            </div>
        </div>
    @endforeach
</div>
