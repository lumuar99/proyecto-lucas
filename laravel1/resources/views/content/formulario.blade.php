<div class="form-group">
    <label for="nombreCliente">Nombre Cliente</label>
    <input type="text" class="form-control" id="nombreCliente" aria-describedby="nombreCliente" name="nombreCliente">
</div>
<div class="form-group">
    <label for="apellidosCliente">Apellidos Cliente</label>
    <input type="text" class="form-control" id="apellidosCliente" name="apellidosCliente">
</div>
<div class="form-group">
    <label for="dnicifCliente">DNI:</label>
    <input type="text" class="form-control" id="dnicifCliente" name="dnicifCliente">
</div>
<div class="form-group">
    <label for="telefonoCliente">Telefono:</label>
    <input type="tel" class="form-control" id="telefonoCliente" name="telefonoCliente">
</div>
<div class="form-group">
    <label for="emailCliente">Email:</label>
    <input type="email" class="form-control" id="emailCliente" name="emailCliente">
</div>
<div class="form-group">
    <label for="categoriaCliente">Categoria:</label>
    <input type="text" class="form-control" id="categoriaCliente" name="categoriaCliente">
</div>
<div class="form-group">
    <label for="comunicaciones_ContenidoCliente">Comunicaciones contenido:</label>
    <input type="text" class="form-control" id="comunicaciones_ContenidoCliente" name="comunicaciones_ContenidoCliente">
</div>
<div class="form-group">
    <label for="comunicaciones_Fecha_hora_ComunicacionCliente">Comunicaciones fecha/hora:</label>
    <input type="text" class="form-control" id="comunicaciones_Fecha_hora_ComunicacionCliente" name="comunicaciones_Fecha_hora_ComunicacionCliente">
</div>
<div class="form-group">
    <label for="medio_comunicacionCliente">Medio de comunicación:</label>
    <input type="text" class="form-control" id="medio_comunicacionCliente" name="medio_comunicacionCliente">
</div>
<div class="form-group">
    <label for="eventos_nombreCliente">Eventos:</label>
    <input type="text" class="form-control" id="eventos_nombreCliente" name="eventos_nombreCliente">
</div>
<div class="form-group">
    <label for="ubicaciones_latitudCliente">Ubicación latitud:</label>
    <input type="text" class="form-control" id="ubicaciones_latitudCliente" name="ubicaciones_latitudCliente">
</div>
<div class="form-group">
    <label for="ubicaciones_longitudCliente">Ubicación longitud:</label>
    <input type="text" class="form-control" id="ubicaciones_longitudCliente" name="ubicaciones_longitudCliente">
</div>
<div class="form-group">
    <label for="ubicaciones_nombreUbicacionCliente">Nombre ubicación:</label>
    <input type="text" class="form-control" id="uubicaciones_nombreUbicacionCliente" name="ubicaciones_nombreUbicacionCliente">
</div>
<button type="submit" class="btn btn-primary">Submit</button>
