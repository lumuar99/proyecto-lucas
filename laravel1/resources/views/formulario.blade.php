<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi formulario Laravel</title>
</head>
<body>
    <form action="/formulario" enctype="multipart/form-data" method="post">
        <input type="text" name="nombre" id="inputNombre" />
        <input type="number" name="edad" id="inputEdad"/>
        <label for="inputSexoHombre">Hombre</label>
        <input type="radio" name="sexo" value="Hombre" id="inputSexoHombre">
        <label for="inputSexoMujer">Mujer</label>
        <input type="radio" name="sexo" value="Mujer" id="inputSexoMujer">
        <input type="checkbox" name="vehicle1" value="Bike"><label for="vehicle1"> I have a bike</label>
        <input type="checkbox" name="vehicle2" value="Car"><label for="vehicle2"> I have a car</label>
        <input type="checkbox" name="vehicle3" value="Boat"><label for="vehicle3"> I have a boat</label>
        <label for="avatar">Choose a profile picture:</label>
        <input type="file" id="avatar" name="avatar" accept="image/png, image/jpeg">
        {{ csrf_field() }}
        <input type="submit" value="Enviar">
    </form>
</body>
</html>
