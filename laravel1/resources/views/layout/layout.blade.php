<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <script src="<?php echo url("jquery-3.5.1.min.js"); ?>"> </script>
    <script src="<?php echo url("bootstrap-4.5.3-dist/js/bootstrap.js"); ?>"> </script>
    <!--<script src="<?php //echo url("js/interfaceLoader.js"); ?>"> </script>-->
    <script src="<?php echo url("js/serviceComm.js"); ?>"> </script>
    <link rel="stylesheet" href="<?php echo url("bootstrap-4.5.3-dist/css/bootstrap.css"); ?>">
    <title>{{ $title ?? 'RutCom' }}</title>
</head>
<body>
    @include('env.topbar')
    <main role="main" class="container">
        @yield('contenido')
    </main>
    @include('env.footer')
</body>
</html>
