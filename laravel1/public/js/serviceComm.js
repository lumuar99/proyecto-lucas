window.onload = createEnv;

function createEnv(){
    document.getElementById("enviar").onclick = function(){
        var method = document.getElementById("method").value;
        var url = document.getElementById("url").value;
        var data = {"nombre":"valor Nombre","apellidos":"valorApellidos"};
        var csrftoken = getHeaderValue("csrf-token");
        getFile(method,url,data,csrftoken,function(data){
            document.getElementById("console").innerHTML = data["data"];
        });
    }
}
function getHeaderValue(headername){
    return document.querySelector('META[name="'+headername+'"]').content;
}
function toHttpParams(data){
    var httpstring="";
    Object.entries(data).forEach(([key,value]) => {
        httpstring += key + "=" + value + "&";
    });
    httpstring=encodeURI(httpstring.slice(0,-1));
    return httpstring;
}
async function getFile(method,url,data,csrftoken,myCallback) {
    let req = new XMLHttpRequest();
    req.open(method, url);
    req.setRequestHeader('X-CSRF-TOKEN',csrftoken);
    let params = toHttpParams(data);
    req.onload=function() {
        if (this.readyState == 4 && this.status == 200) {
            myCallback({
                "status":this.status,
                "data":this.responseText
            });
        } else {
            myCallback({"status":this.status});
        }
    }
    req.send(params);
}