window.onload = loadInterface;

function loadInterface(){
    HTMLElement.prototype.empty = function() {
        while (this.firstChild) {
            this.removeChild(this.firstChild);
        }
    }
    getFile("/getInterfaceDescriptor",createInterfaceFromJson);
}

function getHeaderValue(headername){
    return document.querySelector('META[name="'+headername+'"]').content;
}

async function getFile(method,url,myCallback) {
    let req = new XMLHttpRequest();
    req.open(method, url);
    let csrftoken = getHeaderValue("csrf-token");
    req.setRequestHeader('X-CSRF-TOKEN',csrftoken);
    req.onload=function() {
        if (this.readyState == 4 && this.status == 200) {
            myCallback({
                "status":this.status,
                "data":JSON.parse(this.responseText)});
        } else {
            myCallback({"status":this.status});
        }
    }
    req.send();
}
function getDataArray(){
    return [
        {
            "id":0,
            "videocode":"h6ZbAivhu18",
            "cardtitle":"Testing en frontend: ¿Por qué está roto? ¡Testing Library al rescate!",
            "cardcontent":"Tests que se rompen pero el código funciona, código que se rompe pero los tests pasan… ¿Qué pasa con el testing en frontend? Vamos a ver cómo escribir tests robustos, mantenibles y que nos den confianza con la ayuda de Testing Library 🚀"
        },
        {
            "id":1,
            "videocode":"VW_bAItmWhU",
            "cardtitle":"ANÉCDOTAS de PROGRAMADORES en ON-CALL (y cuánto se cobra)",
            "cardcontent":"No siempre cuando estamos de guardias (on-call) estamos al 100% 🍻🍹😳🍷. Además, vamos a ver cuánto se cobra por ello. 💰👇👇💰"
        }
    ];
}




function expandTemplate(datos, template){

    var numDatos = datos.length;
    var elements = [];
    for (i=0;i<numDatos;i++){
        var newElement=createElementJson(template);
        Object.entries(datos[i]).forEach(([key,value]) => {
            newElement.outerHTML = newElement.outerHTML.split('{'+key+'}').join(value);
        });
        elements.push(newElement);
    }
    return elements
}


function createElementJson(element){
    var newElement = document.createElement(element["element"]);
    newElement.setAttribute("id",element["id"]);
    Object.entries(element["attribute"])
        .forEach(function([nombreatributo,valor]){
            newElement.setAttribute(nombreatributo, valor);
        });
    if (element.hasOwnProperty("content")){
        if (Array.isArray(element["content"])){
            element["content"].forEach(function(contentElement){
                newElement.appendChild(createElementJson(contentElement));
            });
        }else if (typeof element["content"] ==="string"){
            newElement.appendChild(document.createTextNode(element["content"]));
        }
    }
    return newElement;
}

function createInterfaceFromJson(interfaceData){
    if (interfaceData["status"]==200){
        document.body.empty();
        var topbar = createElementJson(interfaceData["data"]["envwrapper"][0]["topbar"]);
        var container = createElementJson(interfaceData["data"]["content"][0]["contentcontainer"]);
        var cardcontainer = createElementJson(interfaceData["data"]["content"][0]["cardscontainer"]);
        var footer = createElementJson(interfaceData["data"]["envwrapper"][0]["footer"]);
        var cardsLists=expandTemplate(getDataArray(), interfaceData["data"]["content"][0]["cardtemplate"]);
        cardsLists.array.forEach(element => {
            cardcontainer.appendChild(element);
        });
        document.body.appendChild(topbar);
        container.appendChild(cardcontainer);
        document.body.appendChild(container);
        document.body.appendChild(footer);
    }
}
