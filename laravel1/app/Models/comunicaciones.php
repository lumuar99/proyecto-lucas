<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class comunicaciones extends Model
{
    /**
     * 
     */
    protected $table = 'ComunicacionesView';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'fechaComunicacion';
    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
        /**
     * The data type of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';
}
