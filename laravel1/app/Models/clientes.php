<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class clientes extends Model{
    /**
     * 
     */
    protected $table = 'ClientesView';
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'dnicifCliente';
    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
        /**
     * The data type of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';
}
