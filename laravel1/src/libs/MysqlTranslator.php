<?php
    namespace RutCom\Libs;
    use Illuminate\Support\Facades\DB;
    class MySqlTranslator implements IDataTranslator{
        public static function translateRequest($Command){
            switch ($Command->getCommandType()){
                case SELECT:
                    $resultQuery = DB::select(SqlSentenceCreator::getSQLQuery($Command));
                case INSERT:
                    $resultQuery = DB::insert(SqlSentenceCreator::getSQLQuery($Command));
                    break;
                case UPDATE:
                    $resultQuery = DB::update(SqlSentenceCreator::getSQLQuery($Command));
                    break;
                case DELETE:
                    $resultQuery =DB::delete(SqlSentenceCreator:: getSQLQuery($Command));
                    break;
            }
            return $resultQuery;
        }
    }
?>
