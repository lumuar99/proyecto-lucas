<?php
define('GLOBALKEY',"%RbWR@kE}f-u5hED8SyR:N0P%X|[3Ou9");
define('GLOBALIV','SHokt545LvtY6e55');

function check_similar($actual, $expected) { 
  if (!is_array($expected) || !is_array($actual) ) 
    return ($actual === $expected); 
  foreach ($expected as $key => $value) { 
    if (!check_similar($actual[$key], $expected[$key])) 
      return false; 
  } 
  foreach ($actual as $key => $value) { 
    if (!check_similar($actual[$key], $expected[$key])) 
      return false; 
  } 
  return true; 
}

function esAsociativo(array $array) {
  return count(array_filter(array_keys($array), 'is_string')) > 0;
}

function joinArray($array, $pegamento=""){
  $output=""; 
  if(esAsociativo($array)){
    foreach($array as $key=>$value){
      $output.=tratarElementoArrayAsoc($key,$value, $pegamento).", ";
    }
    $output=substr($output,0,-2);
  }else{
    $output = implode(", ", $array);
  }
  return $output;
}

function tratarElementoArrayAsoc($key, $value, $pegamento){
    return $key." ". ($pegamento ==" "?"": ($pegamento . " ")). $value;
}

function json_response($data=null, $httpStatus=200){
  header_remove();
  header("Content-Type: application/json");
  http_response_code($httpStatus);
  echo json_encode($data);
  exit();
}


function encriptar($string,$clave){
  $clave_encriptar=base64_decode($clave);
  $encriptado = openssl_encrypt($string, 'aes-256-cbc', $clave_encriptar, 0, GLOBALIV);
  return base64_encode($encriptado);
}
function desencriptar($string,$clave){
  $clave_encriptar=base64_decode($clave);
 // list($encrypted_data, $iv) = explode('::', base64_decode($string), 2);
  return openssl_decrypt(base64_decode($string), 'aes-256-cbc', $clave_encriptar, 0, GLOBALIV);
}

function httpPost($url, $data){
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_POST, true);
    $json = json_encode($data);
    //http_build_query()
    curl_setopt($curl, CURLOPT_POSTFIELDS,$json);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}
?>