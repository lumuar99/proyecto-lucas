<?php
    namespace RutCom\Libs;

    class commandClientes implements ICommand{
        private $query = [];
        public function __construct($id){
           $this->query=[
                "tipo" => SELECT,
                "table"=>"ClientesView",
                "components" =>[
                    "expr"=>"all_fields",
                    "order_expr"=>[
                        "`id`"=>"asc"
                    ],
                    "filters"=>[
                        [
                            "field"=>"id",
                            "value"=>$id,
                            "operator"=>IGUAL,
                        ]
                    ],
                    "limit_expr"=>[
                        "ini"=>0,
                        "total"=>10
                    ]
                ]
            ];

        }
        public function getCommandType(){
            return $this->query["tipo"];
        }
        public function exec(){
            return MySqlTranslator::translateRequest($this);
        }
    }
?>
