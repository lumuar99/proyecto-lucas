<?php
    class dataCommand{
        private $sourceName;
        private $container;


        public function __construct($sourceName, $container){
            $this->sourceName=$sourceName;
            $this->container=$container;
        }

        public function getRequest(){
            return $this->container;
        }

        public function getSourceName(){
            return $this->sourceName;
        }
    }
?>
