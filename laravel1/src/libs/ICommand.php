<?php
    namespace RutCom\Libs;

    interface ICommand{
        public function exec();
    }
?>
