<?php
namespace RutCom\Libs;
class SqlSentenceCreator{
    private static function isSecure($Req){
        $reqStr = strtoupper(json_encode($Req));
        $SqlReservedWords= "/ ACCESSIBLE | ACCOUNT | ACTION | ACTIVE | ADD | ADMIN | AFTER |
            AGAINST | AGGREGATE | ALGORITHM | ALL | ALTER | ALWAYS | ANALYSE | ANALYZE | AND | ANY |
            ARRAY | AS | ASC | ASCII | ASENSITIVE | AT | ATTRIBUTE | AUTOEXTEND_SIZE | AUTO_INCREMENT |
            AVG | AVG_ROW_LENGTH | BACKUP | BEFORE | BEGIN | BETWEEN | BIGINT | BINARY | BINLOG |
            BIT | BLOB | BLOCK | BOOL | BOOLEAN | BOTH | BTREE | BUCKETS | BY | BYTE |  CACHE | CALL |
            CASCADE | CASCADED | CASE | CATALOG_NAME | CHAIN | CHANGE | CHANGED | CHANNEL | CHAR | CHARACTER |
            CHARSET | CHECK | CHECKSUM | CIPHER | CLASS_ORIGIN | CLIENT | CLONE | CLOSE | COALESCE | CODE |
            COLLATE | COLLATION | COLUMN | COLUMNS | COLUMN_FORMAT | COLUMN_NAME | COMMENT | COMMIT |
            COMMITTED | COMPACT | COMPLETION | COMPONENT | COMPRESSED | COMPRESSION | CONCURRENT |
            CONDITION | CONNECTION | CONSISTENT | CONSTRAINT | CONSTRAINT_CATALOG | CONSTRAINT_NAME |
            CONSTRAINT_SCHEMA | CONTAINS | CONTEXT | CONTINUE | CONVERT | CPU | CREATE | CROSS | CUBE |
            CUME_DIST | CURRENT | CURRENT_DATE | CURRENT_TIME | CURRENT_TIMESTAMP | CURRENT_USER | CURSOR |
            CURSOR_NAME | DATA | DATABASE | DATABASES | DATAFILE | DATE | DATETIME | DAY | DAY_HOUR |
            DAY_MICROSECOND | DAY_MINUTE | DAY_SECOND | DEALLOCATE | DEC | DECIMAL | DECLARE | DEFAULT |
            DEFAULT_AUTH | DEFINER | DEFINITION | DELAYED | DELAY_KEY_WRITE | DELETE | DENSE_RANK | DESC |
            DESCRIBE | DESCRIPTION | DES_KEY_FILE | DETERMINISTIC | DIAGNOSTICS | DIRECTORY | DISABLE |
            DISCARD | DISK | DISTINCT | DISTINCTROW | DIV | DO | DOUBLE | DROP | DUAL | DUMPFILE | DUPLICATE |
            DYNAMIC | EACH | ELSE | ELSEIF | EMPTY | ENABLE | ENCLOSED | ENCRYPTION | END | ENDS | ENFORCED |
            ENGINE | ENGINES | ENGINE_ATTRIBUTE | ENUM | ERROR | ERRORS | ESCAPE | ESCAPED | EVENT | EVENTS |
            EVERY | EXCEPT | EXCHANGE | EXCLUDE | EXECUTE | EXISTS | EXIT | EXPANSION | EXPIRE | EXPLAIN |
            EXPORT | EXTENDED | EXTENT_SIZE |  FAILED_LOGIN_ATTEMPTS | FALSE | FAST | FAULTS | FETCH |
            FIELDS | FILE | FILE_BLOCK_SIZE | FILTER | FIRST | FIRST_VALUE | FIXED | FLOAT | FLOAT4 |
            FLOAT8 | FLUSH | FOLLOWING | FOLLOWS | FOR | FORCE | FOREIGN | FORMAT | FOUND | FROM | FULL |
            FULLTEXT | FUNCTION |  GENERAL | GENERATED | GEOMCOLLECTION | GEOMETRY | GEOMETRYCOLLECTION |
            GET | GET_FORMAT | GET_MASTER_PUBLIC_KEY | GLOBAL | GRANT | GRANTS | GROUP | GROUPING | GROUPS |
            GROUP_REPLICATION | HANDLER | HASH | HAVING | HELP | HIGH_PRIORITY | HISTOGRAM | HISTORY |
            HOST | HOSTS | HOUR | HOUR_MICROSECOND | HOUR_MINUTE | HOUR_SECOND | IDENTIFIED | IF | IGNORE |
            IGNORE_SERVER_IDS | IMPORT | IN | INACTIVE | INDEX | INDEXES | INFILE | INITIAL_SIZE | INNER |
            INOUT | INSENSITIVE | INSERT | INSERT_METHOD | INSTALL | INSTANCE | INT | INT1 | INT2 | INT3 |
            INT4 | INT8 | INTEGER | INTERVAL | INTO | INVISIBLE | INVOKER | IO | IO_AFTER_GTIDS |
            IO_BEFORE_GTIDS | IO_THREAD | IPC | IS | ISOLATION | ISSUER | ITERATE |  JOIN | JSON | JSON_TABLE |
            JSON_VALUE | KEY | KEYS | KEY_BLOCK_SIZE | KILL | LAG | LANGUAGE | LAST | LAST_VALUE | LATERAL |
            LEAD | LEADING | LEAVE | LEAVES | LEFT | LESS | LEVEL | LIKE | LIMIT | LINEAR | LINES | LINESTRING |
            LIST | LOAD | LOCAL | LOCALTIME | LOCALTIMESTAMP | LOCK | LOCKED | LOCKS | LOGFILE | LOGS | LONG |
            LONGBLOB | LONGTEXT | LOOP | LOW_PRIORITY | MANAGED | MASTER | MASTER_AUTO_POSITION | MASTER_BIND |
            MASTER_COMPRESSION_ALGORITHMS | MASTER_CONNECT_RETRY | MASTER_DELAY | MASTER_HEARTBEAT_PERIOD |
            MASTER_HOST | MASTER_LOG_FILE | MASTER_LOG_POS | MASTER_PASSWORD | MASTER_PORT | MASTER_PUBLIC_KEY_PATH |
                MASTER_RETRY_COUNT | MASTER_SERVER_ID | MASTER_SSL | MASTER_SSL_CA | MASTER_SSL_CAPATH | MASTER_SSL_CERT |
            ASTER_SSL_CIPHER | MASTER_SSL_CRL | MASTER_SSL_CRLPATH | MASTER_SSL_KEY | MASTER_SSL_VERIFY_SERVER_CERT |
            MASTER_TLS_CIPHERSUITES | MASTER_TLS_VERSION | MASTER_USER | MASTER_ZSTD_COMPRESSION_LEVEL | MATCH |
        MAXVALUE | MAX_CONNECTIONS_PER_HOUR | MAX_QUERIES_PER_HOUR | MAX_ROWS | MAX_SIZE | MAX_UPDATES_PER_HOUR |
            MAX_USER_CONNECTIONS | MEDIUM | MEDIUMBLOB | MEDIUMINT | MEDIUMTEXT | MEMBER | MEMORY | MERGE |
            MESSAGE_TEXT | MICROSECOND | MIDDLEINT | MIGRATE | MINUTE | MINUTE_MICROSECOND | MINUTE_SECOND |
            MIN_ROWS | MOD | MODE | MODIFIES | MODIFY | MONTH | MULTILINESTRING | MULTIPOINT | MULTIPOLYGON |
            MUTEX | MYSQL_ERRNO | NAME | NAMES | NATIONAL | NATURAL | NCHAR | NDB | NDBCLUSTER | NESTED |
            NETWORK_NAMESPACE | NEVER | NEW | NEXT | NO | NODEGROUP | NONE | NOT | NOWAIT | NO_WAIT |
            NO_WRITE_TO_BINLOG | NTH_VALUE | NTILE | NULL | NULLS | NUMBER | NUMERIC | NVARCHAR | OF |
            OFF | OFFSET | OJ | OLD | ON | ONE | ONLY | OPEN | OPTIMIZE | OPTIMIZER_COSTS | OPTION |
            OPTIONAL | OPTIONALLY | OPTIONS | OR | ORDER | ORDINALITY | ORGANIZATION | OTHERS | OUT |
            OUTER | OUTFILE | OVER | OWNER | PACK_KEYS | PAGE | PARSER | PARTIAL | PARTITION |
            PARTITIONING | PARTITIONS | PASSWORD | PASSWORD_LOCK_TIME | PATH | PERCENT_RANK | PERSIST |
            PERSIST_ONLY | PHASE | PLUGIN | PLUGINS | PLUGIN_DIR | POINT | POLYGON | PORT | PRECEDES |
            PRECEDING | PRECISION | PREPARE | PRESERVE | PREV | PRIMARY | PRIVILEGES | PRIVILEGE_CHECKS_USER |
            PROCEDURE | PROCESS | PROCESSLIST | PROFILE | PROFILES | PROXY | PURGE | QUARTER | QUERY |
            QUICK | RANDOM | RANGE | RANK | READ | READS | READ_ONLY | READ_WRITE | REAL | REBUILD |
            RECOVER | RECURSIVE | REDOFILE | REDO_BUFFER_SIZE | REDUNDANT | REFERENCE | REFERENCES | REGEXP |
            RELAY | RELAYLOG | RELAY_LOG_FILE | RELAY_LOG_POS | RELAY_THREAD | RELEASE | RELOAD | REMOTE |
            REMOVE | RENAME | REORGANIZE | REPAIR | REPEAT | REPEATABLE | REPLACE | REPLICATE_DO_DB |
            REPLICATE_DO_TABLE | REPLICATE_IGNORE_DB | REPLICATE_IGNORE_TABLE | REPLICATE_REWRITE_DB |
            REPLICATE_WILD_DO_TABLE | REPLICATE_WILD_IGNORE_TABLE | REPLICATION | REQUIRE | REQUIRE_ROW_FORMAT |
            RESET | RESIGNAL | RESOURCE | RESPECT | RESTART | RESTORE | RESTRICT | RESUME | RETAIN | RETURN |
            RETURNED_SQLSTATE | RETURNING | RETURNS | REUSE | REVERSE | REVOKE | RIGHT | RLIKE | ROLE | ROLLBACK |
            ROLLUP | ROTATE | ROUTINE | ROW | ROWS | ROW_COUNT | ROW_FORMAT | ROW_NUMBER | RTREE | SAVEPOINT |
            SCHEDULE | SCHEMA | SCHEMAS | SCHEMA_NAME | SECOND | SECONDARY | SECONDARY_ENGINE |
            SECONDARY_ENGINE_ATTRIBUTE | SECONDARY_LOAD | SECONDARY_UNLOAD | SECOND_MICROSECOND |
            SECURITY | SELECT | SENSITIVE | SEPARATOR | SERIAL | SERIALIZABLE | SERVER | SESSION |
            SET | SHARE | SHOW | SHUTDOWN | SIGNAL | SIGNED | SIMPLE | SKIP | SLAVE | SLOW | SMALLINT |
            SNAPSHOT | SOCKET | SOME | SONAME | SOUNDS | SOURCE | SPATIAL | SPECIFIC | SQL | SQLEXCEPTION |
            SQLSTATE | SQLWARNING | SQL_AFTER_GTIDS | SQL_AFTER_MTS_GAPS | SQL_BEFORE_GTIDS | SQL_BIG_RESULT |
            SQL_BUFFER_RESULT | SQL_CACHE | SQL_CALC_FOUND_ROWS | SQL_NO_CACHE | SQL_SMALL_RESULT | SQL_THREAD |
            SQL_TSI_DAY | SQL_TSI_HOUR | SQL_TSI_MINUTE | SQL_TSI_MONTH | SQL_TSI_QUARTER | SQL_TSI_SECOND |
            SQL_TSI_WEEK | SQL_TSI_YEAR | SRID | SSL | STACKED | START | STARTING | STARTS | STATS_AUTO_RECALC |
            STATS_PERSISTENT | STATS_SAMPLE_PAGES | STATUS | STOP | STORAGE | STORED | STRAIGHT_JOIN | STREAM |
            STRING | SUBCLASS_ORIGIN | SUBJECT | SUBPARTITION | SUBPARTITIONS | SUPER | SUSPEND | SWAPS | SWITCHES |
            SYSTEM | TABLE | TABLES | TABLESPACE | TABLE_CHECKSUM | TABLE_NAME | TEMPORARY | TEMPTABLE |
            TERMINATED | TEXT | THAN | THEN | THREAD_PRIORITY | TIES | TIME | TIMESTAMP | TIMESTAMPADD |
            TIMESTAMPDIFF | TINYBLOB | TINYINT | TINYTEXT | TLS | TO | TRAILING | TRANSACTION | TRIGGER |
            TRIGGERS | TRUE | TRUNCATE | TYPE | TYPES | UNBOUNDED | UNCOMMITTED | UNDEFINED | UNDO |
            UNDOFILE | UNDO_BUFFER_SIZE | UNICODE | UNINSTALL | UNION | UNIQUE | UNKNOWN | UNLOCK | UNSIGNED |
            UNTIL | UPDATE | UPGRADE | USAGE | USE | USER | USER_RESOURCES | USE_FRM | USING | UTC_DATE |
            UTC_TIME | UTC_TIMESTAMP | VALIDATION | VALUE | VALUES | VARBINARY | VARCHAR | VARCHARACTER |
            VARIABLES | VARYING | VCPU | VIEW | VIRTUAL | VISIBLE | WAIT | WARNINGS | WEEK | WEIGHT_STRING |
            WHEN | WHERE | WHILE | WINDOW | WITH | WITHOUT | WORK | WRAPPER | WRITE | X509 | XA | XID | XML |
            XOR | YEAR | YEAR_MONTH | ZEROFILL | AUTHORIZATION | BREAK | BROWSE | BULK | CHECKPOINT |
            CLUSTERED | COMPUTE | CONTAINSTABLE | DBCC | DENY | DISTRIBUTED | DUMMY | DUMP | ERRLVL | EXEC |
            EXTERNAL | FILLFACTOR | FREETEXT | FREETEXTTABLE | GOTO | HOLDLOCK | IDENTITY | IDENTITY_INSERT |
            IDENTITYCOL | INTERSECT | LINENO | NOCHECK | NONCLUSTERED | NULLIF | OFFSETS | OPENDATASOURCE |
            OPENQUERY | OPENROWSET | OPENXML | PERCENT | PIVOT | PLAN | PRINT | PROC | PUBLIC | RAISERROR |
            READTEXT | RECONFIGURE | REVERT | ROWCOUNT | ROWGUIDCOL | RULE | SAVE | SESSION_USER | SETUSER |
            STATISTICS | SYSTEM_USER | TABLESAMPLE | TEXTSIZE | TOP | TRAN |
            TSEQUAL | UNPIVOT | UPDATETEXT | WAITFOR | WRITETEXT/";
         $found=[];
         $result = preg_match_all($SqlReservedWords, $reqStr, $found);

        return (!is_bool($result) && !$result);

     }

    public static function getSQLQuery($query_components){
        $query=null;
        $sqlSentenceCreatorIntance = new SqlSentenceCreator();
        if(is_array($query_components) && count($query_components)>0){
            if ($sqlSentenceCreatorIntance->isSecure($query_components)){
                $tipoquery=$sqlSentenceCreatorIntance->getCommandType($query_components);
                switch($tipoquery){
                    CASE SELECT:
                    $query=$sqlSentenceCreatorIntance->createSelect($query_components);
                    break;
                    CASE INSERT:
                    $query=$sqlSentenceCreatorIntance->createInsert($query_components);
                    break;
                    CASE UPDATE:
                    $query=$sqlSentenceCreatorIntance->createUpdate($query_components);
                    break;
                    CASE DELETE:
                    $query=$sqlSentenceCreatorIntance->createDelete($query_components);
                    break;
                    default:
                    $query = null;
                    break;
                }
            }
        }
        return $query;
    }
    private function createSelect($query_components){
        $consultaFinal= null;
        if($this->validatePackage($query_components)){
            $inicioConsulta="SELECT";
            $campos=$this->createComponents($query_components["components"]["expr"]);
            $table= "FROM " .$this->createTable($query_components["table"]);
            $consultaFinal=$inicioConsulta." ".$campos." ".$table;
            if (isset($query_components["components"]["filters"])) {
                $consultaFinal.=" WHERE ".$this->createWhere($query_components["components"]["filters"]);
            }
            if (isset($query_components["components"]["order_expr"])) {
                $consultaFinal.=" ORDER BY ".$this->createOrderComponents($query_components["components"]["order_expr"]);
            }
            if (isset($query_components["components"]["limit_expr"])) {
                $consultaFinal.=" LIMIT ".$this->createLimitComponents($query_components["components"]["limit_expr"]);
            }
            $consultaFinal.=";";
        }
        return $consultaFinal;
    }
    private function createInsert($query_components){
        $consultaFinal= null;
        if($this->validatePackage($query_components)){
            $inicioConsulta="INSERT ";
            $table= "INTO " . $this->createTable($query_components["table"]);
            $consultaFinal=$inicioConsulta.$table." ".$this->createInsertExpresion($query_components["components"]);
        }
        return $consultaFinal;
    }
    private function createUpdate($query_components){
        $consultaFinal= null;
        if($this->validatePackage($query_components)){
            $inicioConsulta="UPDATE ";
            $table= $this->createTable($query_components["table"]);
            $campos=$this->createSet($query_components["components"]["expr"]);
            $where=$this->createWhere($query_components["components"]["filters"]);
            $consultaFinal=$inicioConsulta.$table." SET ".$campos." WHERE ".$where.";";
        }
        return $consultaFinal;
    }
    private function createDelete($query_components){
        $consultaFinal= null;
        if($this->validatePackage($query_components)){
            $inicioConsulta="DELETE ";
            $table= $this->createTable($query_components["table"]);
            $where=$this->createWhere($query_components["components"]["filters"]);
            $consultaFinal=$inicioConsulta."FROM ".$table." WHERE ".$where.";";
        }
        return $consultaFinal;
    }
    private function createSet($componentsExpr){
        $resultado="";
        foreach ($componentsExpr as $key => $value) {
            $campos=$this->createField($key);
            $valores=$this->createValue($value);
            $resultado.=$campos."=".$valores.",";
        }
        $resultado=substr($resultado,0,-1);
        return $resultado;

    }
    private function createInsertExpresion($componentsExpr){
        $resultado="";
        $campos = "";
        $valores="";
        foreach ($componentsExpr["insertarCampos_expr"] as $value)
            $campos.=$this->createField($value).',';
        foreach ($componentsExpr["insertarValores_expr"] as $value)
            $valores.=$this->createValue($value). ',';

        $campos=substr($campos,0,-1);
        $valores=substr($valores,0,-1);
        $resultado = '('.$campos . ") VALUES (". $valores . ");";
        return $resultado;

    }

    private function createComponents($campos){
        if(is_array($campos) && count($campos) > 0 ){
            $resultado=joinArray($campos, esAsociativo($campos)?"AS":"");
        }elseif($campos == "all_fields"){
            $resultado= "*";
        }else{
            $resultado= null;
        }
        return $resultado;
    }
    private function createTable($table){
        $resultado = null;
        if(is_string($table) && strlen($table) > 0){
            $resultado="`$table`";
        }
        return $resultado;
    }
    private function createOrderComponents($order){
        $resultado= null;
        if(is_array($order) && count($order) > 0 ){
           $resultado= joinarray($order," ");
        }
        return $resultado;
    }
    private function createLimitComponents($limit){
        $resultado= null;
        if(is_array($limit)){
            if(key_exists('ini',$limit) && key_exists('total',$limit)) {
                $resultado=$limit['ini'].",".$limit['total'];
            }
        }
        return $resultado;
    }
    public static function getCommandType($query_components){
        $arrayType = [SELECT,INSERT,UPDATE,DELETE];
        $resultado=null;
        if(is_array($query_components) && count($query_components) > 0){
            if(isset($query_components["tipo"]))
                if(in_array($query_components["tipo"], $arrayType))
                    $resultado= $query_components["tipo"];
        }
        return $resultado;
    }
    private function createWhere($whereArray){
        $resultado ="";
        if(is_array($whereArray) && count($whereArray) > 0){
            for($i=0;$i<count($whereArray);$i++){
                $resultado.=$this->createFilter($whereArray,$i);
            }
        }else{
            $resultado = null;
        }
        return $resultado;
    }
    private function createFilter($filters,$i){
        $whereFinal= null;
        $cFilter=$filters[$i];
        if($this->checkFilter($cFilter)){
            $campo= $this->createField($cFilter["field"]);
            $operador= $this->getConsTraductor($cFilter["operator"]);
            $valor= $this->createValue($cFilter["value"]);
            $whereFinal= $campo." ".$operador." ".$valor;
            if(isset($cFilter["conj"]) && isset($filters[$i+1])){
                $conjuncion= $this->getConsTraductor($cFilter["conj"]);
                $whereFinal.= " $conjuncion ";
            }
        }
        return $whereFinal;
    }
    private function checkFilter($cFilter){
        $condiciones = [
            is_array($cFilter),
            count($cFilter) > 0,
            isset($cFilter["field"]),
            isset($cFilter["value"]),
            isset($cFilter["operator"]),
            $cFilter["field"] != $cFilter["value"]
        ];
        $i=0;
        while($condiciones[$i]) $i++;
        return ($i==count($condiciones));
    }
    private function createValue($value){
        $valueFinal=null;
        if(is_numeric($value))
            $valueFinal=$value;
        elseif (is_string($value) && strlen($value)>0)
            $valueFinal="'".$value."'";
        return $valueFinal;
    }
    private function createField($field){
        $fieldFinal= null;
        if(is_string($field) && strlen($field)>0){
            $fieldFinal="`".$field."`";
        }
        return $fieldFinal;
    }
    private function getConsTraductor($constante){
        $result = null;
        $constanteTotal = [
            IGUAL=>"=",
            MAYOR=>">",
            MENOR=>"<",
            MENORIGUAL=>"<=",
            MAYORIGUAL=>">=",
            DISTINTO=>"<>",
            ENTRE=>"BETWEEN",
            COMO=>"LIKE",
            EN=>"IN",
            Y=>"AND",
            O=>"OR",
            NO=>"NOT"
        ];

        if(is_string($constante) && array_key_exists($constante, $constanteTotal)){
            $result = $constanteTotal[$constante];
        }
        return $result;
    }
    private function validatePackage($query_components){
        $condiciones_obligatorias=[
            isset($query_components["tipo"]),
            isset($query_components["table"]),
            isset($query_components["components"]),
            is_string($query_components["tipo"])?strlen($query_components["tipo"])>0:false,
            is_string($query_components["table"])?strlen($query_components["table"])>0:false,
            is_array($query_components["components"])?count($query_components["components"])>0:false
        ];
        $tipo = $this->getCommandType($query_components);
        switch ($tipo){
            case SELECT:
            $condiciones=array_merge(
                $condiciones_obligatorias,
                $this->validatePackageSelect($query_components)
            );
            break;
            case INSERT:
            $condiciones=array_merge(
                $condiciones_obligatorias,
                $this->validatePackageInsert($query_components) /**@TODO:  Carlos*/
            );
            break;
            case UPDATE:
            $condiciones=array_merge(
                $condiciones_obligatorias,
                $this->validatePackageUpdate($query_components)/**@TODO:  Alex y alberto*/
            );
            break;
            case DELETE:
            $condiciones=array_merge(
                $condiciones_obligatorias,
                $this->validatePackageDelete($query_components)/**@TODO: David Y José */
            );
            break;
            default:
            $condiciones=null;
            break;
        }
        $i=0;
        while($condiciones[$i]) $i++;
        return ($i==count($condiciones));
    }
    private function validatePackageSelect($query_components){
        $condicionesReq_select=isset($query_components["components"]["expr"]);
        if (!$condicionesReq_select)
            return [$condicionesReq_select];

        if(isset($query_components["components"]["filters"])){
            if(!$this->validatePackageWhere($query_components["components"]["filters"])){
                return [false];
            }
        }
        if(isset($query_components["components"]["order_expr"])){
            if(!$this->validateSelectOrder($query_components["components"]["order_expr"])){
                return [false];
            }
        }
        if(isset($query_components["components"]["limit_expr"])){
            if(!$this->validateSelectLimit($query_components["components"]["limit_expr"])){
                return [false];
            }
        }
        return [$condicionesReq_select];
    }
    private function validateSelectOrder($arrayOrder){
        $validate = is_array($arrayOrder) && count($arrayOrder)>0;
        if ($validate){
            foreach ($arrayOrder as $key=>$value){
                $validate = isset($arrayOrder[$key]) && is_string($arrayOrder[$key])  && ($value=="asc" || $value=="desc");
                if (!$validate) return false;
            }
        }
        return $validate;
    }
    private function validateSelectLimit($arrayLimit){
        $validate=is_array($arrayLimit) && count($arrayLimit)>0;
        if($validate){
            foreach ($arrayLimit as $key=>$value){
                $validate= isset($arrayLimit[$key]) && ($key=="ini" || $key=="total") && is_int($value);
                if(!$validate) return false;
            }
        }
        return $validate;
    }
    private function validatePackageInsert($query_components){
        $isArrayCampos = false;
        $isArrayValores =false;
        $validContentCampos = false;
        $validContentValores = false;
        if (isset($query_components["components"]["insertarCampos_expr"])){
            $isArrayCampos = is_array($query_components["components"]["insertarCampos_expr"]);
            $validContentCampos = $this->validateContentInsert($query_components["components"]["insertarCampos_expr"]);
        }

        if(isset($query_components["components"]["insertarValores_expr"])){
            $isArrayValores = is_array($query_components["components"]["insertarValores_expr"]);
            $validContentValores = $this->validateContentInsert($query_components["components"]["insertarValores_expr"]);
        }

        $nCampos = $isArrayCampos?count($query_components["components"]["insertarCampos_expr"]):0;
        $nValores = $isArrayValores?count($query_components["components"]["insertarValores_expr"]):0;

        $condicionesReq_insert=[
            $isArrayCampos,
            $isArrayValores,
            $nCampos>0,
            $nValores>0,
            $validContentCampos,
            $validContentValores,
            $nValores==$nCampos
        ];
        return $condicionesReq_insert;
    }
    private function validateContentInsert($arrayContent){
        if(is_array($arrayContent) && count($arrayContent)>0){
            $valid = true;
            for ($i=0;$i<count($arrayContent);$i++){
                $valid = $valid && !is_array($arrayContent[$i]);
                if(!$valid)
                    return $valid;
            }
        }
        return $valid;
    }
    private function validatePackageUpdate($query_components){
        $condicionesReq_update = isset($query_components["components"]["expr"]) &&
                                  isset($query_components["components"]["filters"]);
        if(!$condicionesReq_update){
            return [$condicionesReq_update];
        }
        foreach($query_components["components"]["expr"] as $key=>$value){
            if((strlen($key)==0 || is_int($key)) && (!is_string($value) || !is_numeric($value)))
                return [false];
        }
        return [$this->validatePackageWhere($query_components["components"]["filters"])];
    }
    private function validatePackageDelete($query_components){
        return [
            isset($query_components["components"]["filters"]) &&
            $this->validatePackageWhere($query_components["components"]["filters"])
        ];
    }
    private function validatePackageWhere($arrayWhere){
        $validate=is_array($arrayWhere) && count($arrayWhere)>0;
        if($validate){
            for ($i=0; $i<count($arrayWhere); $i++){
                $validate= isset($arrayWhere[$i]["field"]) &&
                        isset($arrayWhere[$i]["value"]) &&
                        ($arrayWhere[$i]["field"] != $arrayWhere[$i]["value"]) &&
                        isset($arrayWhere[$i]["operator"]) &&
                        is_string($arrayWhere[$i]["field"]) &&
                        (is_int($arrayWhere[$i]["value"]) || is_string($arrayWhere[$i]["value"]))&&
                        null!==$this->getConsTraductor($arrayWhere[$i]["operator"]);
                if (!$validate) return false;
                if (isset($arrayWhere[$i+1]) && !isset($arrayWhere[$i]["conj"]) &&  null===$this->getConsTraductor($arrayWhere[$i]["conj"]))
                    return false;
            }
        }
        return $validate;
    }
}
?>
